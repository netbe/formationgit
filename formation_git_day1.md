footer: Formation GIT - Adonya - François Benaiteau
slidenumbers: true
autoscale: true
build-lists: true

# [fit] Formation GIT
## LIMPIDE
### 1/2
---
# Problème

- sauvegarde du projet
- revenir sur une version donnée
- convention de nommage et informations
- collaborer sur un projet

---
# Problème

![inline, fill, 100%](without_git_dates.png)  ![inline fill, 100%](without_git_versions.png)


**Utilisation de dates ou version**

---

# What is GIT ?

A distributed **Version Control System**

## Different Version Control Systems (VCS)

- Local VCS: local db that keeps patch of files
- Centralised VCS: Version Database on a server (examples: CVS, SVN, and Perforce)
- Distributed VCS: Sync db between local machine and server

---
# Distributed VCS

* Sync db between local machine and server
* Examples: **Git**, Mercurial, Bazaar or Darcs

![right, 90%](distributed.png)

---

# Basics

## Installation

_Note: git est installé par défaut sur macOS (`version 2.21.1`)._

On peut utiliser **GIT** via plusieurs interfaces:
- ligne de commande : `git ... (man git)`
- [sourceTree](https://www.sourcetreeapp.com/)
- [GitUp](https://gitup.co/)
- [tig](https://jonas.github.io/tig/)
- ...

---

# Basics
## Init

Nouveau projet git
```
mkdir myRepo
cd myRepo
git init .
```
x
---

# Basics
## Init

Fichiers cachés dans `.git`

![Right, fit](git_folder.png)

---
# Basics
## Add/Remove/Rename

Ajouter un fichier du repository
`git add toto.txt`
Renommer ou déplacer un fichier du repository
`git mv toto.txt tata.txt`
Supprimer un fichier du repository
`git rm toto.txt`

---
# Basics
## Status

Voir l'état de tous les fichiers versionnés
`git status`


---
# Basics
## Commit

Créer une nouvelle version locale (sauvegarde)
`git commit -m "Ma nouvelle version"`

---
# Basics
## Checkout

Créer une branche du projet et la séléctionner
`git checkout -b develop`

Sélectionner une branche du projet
`git checkout master`

---
# Basics
## Clone

Récupérer un projet existant
`git clone git://core.git.wordpress.org/`

---
# Basics
## Ajout de remote

Lier le depot local à un depot distant (serveur)
`git remote add origin git@github.com:netbe/FormationGit.git`

* origin -> nom local du serveur (défaut: origin)
* url soit en HTTPS ou SSH

---
# Basics
## Push

Transmettre les versions locales au serveur
`git push <ref distant> <nom de la branche>`

Exemple :
```
git push origin master
```

---
# Basics
## Pull

Récupérer les versions distantes du serveur et mettre à jour la branche courante
`git pull origin <nom de la branche>`

Exemple :
```
git pull origin master
```

---
# Basics
## Fetch

Récupérer les changements du serveur
`git fetch origin`


---
# Basics
## Revert

Annuler les changements d'une version (commit)
`git revert HEAD`


---
# Basics
## Reset

Effacer les changements en cours et retourner à la dernière version en cours
`git reset --hard HEAD`

---
# Basics
## Tag

Ajouter un label à une version (commit) donné
`git tag 1.0`

`git tag -a 1.0 -m "First Release to production"`

---
# Basics
## Merge

[.column]
* Fusionner (merge) le contenu d'une version à la version actuelle (`HEAD`).
* Un commit contenant la difference entre les deux versions.

`git merge <commit>`

Exemple: 

`git merge modification`

[.column]
![inline](merge2.png)

![inline](merge1.png)

---
# Basics
## Merge et conflit 

![inline, left](merge_conflit.png)![right, 75%](filemerge.png)

---
# Ignore Files

* Fichier `.gitignore` contenant la liste de fichiers à ne pas sauvegarder.

Exemple: `https://www.gitignore.io/api/wordpress`

---
# Git flow

* Méthodologie de travail
* Nommage des branches


---
# Git flow [^1]
## Feature branches

* `brew install git-flow`
* Nouveau développement (new features, bug fix non critique...)
* Convention de nommage: `feature/XXX`

[^1]: https://datasift.github.io/gitflow/IntroducingGitFlow.html

---
![inline, 76%](GitFlowFeatureBranches.png)

---
![inline, 76%](GitFlowDevelopBranch.png)

---
# Git flow [^1]
## Release branches

* Nouvelle version planifiée à deployer
* Convention de nommage: `release/XXX`

---
![inline, 76%](GitFlowReleaseBranch.png)

---
![inline, 76%](GitFlowMasterBranch.png)

---
# Git flow [^1]
## Hotfix branches

* Correctif par rapport a une version déployée (à partir d'un tag sur master)
* Convention de nommage: `hotfix/XXX`

---
![inline, 76%](GitFlowHotfixBranch.png)

