footer: Formation GIT - Adonya - François Benaiteau
slidenumbers: true
autoscale: true
build-lists: true

# [fit] Formation GIT
## LIMPIDE
### 2/2

---
# Advanced
## Merge

* Qu'est ce que cela veut dire `fast-forward merge` ?
* Exemple `fast-forward`

```
git checkout develop
git merge feature/toto
git merge feature/toto --no-ff
git merge feature/toto --no-commit --no-ff
```

---
# Advanced
## Merge

Forcer non-fast-forward et ne pas commiter

```
git config --global merge.commit no
git config --global merge.ff no
```

---
# Advanced
## Rebase

`git rebase -i`

* Commande pour réécrire l'historique GIT (commits)
* Determine l'action à effectuer pour chaque commit (`pick, edit, reword...`)

---
# Advanced

* Fixup: Merge le contenu du commit sélectionné avec le précédent
* Squash: Merge le contenu et le message du commit sélectionné avec le précédent

---
# Advanced
## Exercice

* `test-cdn`
1. Regrouper les commits avec le meme message
2. Séparer en deux commits le commit "mobile version done, starting desktoip version"
3. Réécrire les messages de commit pour enlever les fautes de frappe. Les messages commencent par une majuscule (+ verbe a l'impératif)
4. Pousser vos changements sur un fork ou propre dépôt distant 


---
# Advanced
## Rebase vs Merge

* Merge les branches connues 
* Rebase
    * les branches temporaires, dont l'historique n'importe peu
    * Lorsque c'est plus simple qu'un merge (cherry-pick, squash)


---
# Advanced
## Autres

* `git stash save` Sauvegarder l'état actuel du `workspace` copie de dépôt
* `git blame` - Retrouver le commit contenant un bug particulier.
* `git submodule add <repository> <path>`

---
# Advanced
## Git hooks

* Dans le dossier `.git/hooks`
* Scripts qui sont exécutés après/avant certaines actions git
* Cote serveur: `pre-receive`, `post-receive`
* Cote client: `pre-push`

---
# Advanced
## Git hooks

* Exemples: `test-cdn`, `phpMerge`


---
# Pipeline

* fichier `bitbucket-pipelines.yml`
* Exemple: deploy wordpress

---
# Wordpress
## Deployment tools

* [Deployer](https://deployer.org/) - [blog post](https://blog.kulakowski.fr/post/deployer-votre-application-php-avec-deployer)
* [Capistrano](https://capistranorb.com/)
* [WP-Deploy](https://github.com/Mixd/wp-deploy)
* https://wp-cli.org/


---
# Wordpress
## Git ftp

```
image: php:7.1.29

pipelines:
 branches:
   master:
     - step:
         name: Deploy to production
         deployment: production
         script:
           - apt-get update
           - apt-get -qq install git-ftp
           - git ftp push --user $FTP_username --passwd $FTP_password ftp://ftp.clientsite.com
   develop:
     - step:
         name: Deploy to staging
         deployment: staging
         script:
           - apt-get update
           - apt-get -qq install git-ftp
           - git ftp push --user $FTP_username --passwd $FTP_password ftp://ftp.clientsite.com/staging.clientsite.com
```
[Source](https://www.toptal.com/wordpress/bitbucket-wordpress-version-control)
