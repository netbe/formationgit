footer: Formation GIT - François Benaiteau
slidenumbers: true
autoscale: true
build-lists: true

# Exercice 1

1. Créer un nouveau depot git
2. Ajouter un fichier toto.txt avec comme contenu "Hello"
3. Commiter ce fichier
4. Renommer le fichier en francois_benaiteau.txt puis commiter le fichier
5. Supprimer le fichier francois_benaiteau.txt
6. Creer une nouvelle branche avec vos initiales et mettre un fichier a votre nom contenant du texte

---
# Exercise 2 (1/4) 
- Créer un nouveau dépôt (repository)
- Créer un nouveau fichier `formateurs.csv` comprenant le prénom, nom, et la profession de chaque formateurs.
```
prénom, nom, profession
François, Benaiteau, Formateur GIT
```
- Pousser (_Push_) une version sur le serveur distant BitBucket

---
# Exercise 2 (2/4)
- Ajouter un fichier `stagiaires.csv` comprenant le prénom, nom, et la profession de chaque stagiaire.

---
# Exercise 2 (3/4)
- Modifier les fichiers pour n'avoir qu'un seul fichier `personnes.csv` qui regrouperait stagiaires et formateurs
- Ajouter un champ `role` dans le fichier `csv` pour distinguer stagiaires et formateurs.

---
# Exercise 2 (4/4)
- Ajouter depuis BitBucket, le fichier `script.php` suivant:

```
<?php
 $csvFile = file('stagiaires.csv');
    $data = [];
    foreach ($csvFile as $line) {
        $data[] = str_getcsv($line);
    }

    foreach ($data as $key=> $person) {
        if($key == 0) {
            continue;
        }
        echo($person[0] . " " . $person[1] .  " est " . $person[2] . "\n");
    }
?>
```
- Récupérer ce fichier localement (_Pull_)
- Modifier le script pour qu'il lise le fichier `personnes.csv`

---
# Exercise 3 - Fork

On veut créer un site contenant la presentation de chacun des membres de la formation.
Le projet est entamé à l'adresse: [https://bitbucket.org/netbe/presentationgit/src/master/](https://bitbucket.org/netbe/presentationgit/src/master/)

* Ajouter chacun votre description sur la même branche
* Créer une nouvelle branche `contact` qui permet d'écrire un mail à chaque membre
* Créer une PR, après revue de code, merger le code

---
# Exercise 4

- Créer un nouveau site Wordpress 
- Le mettre sous git
- Configurer le site
- Ignorer les fichiers ne relevant pas du code
- Partager le code sur un bitbucket

---
# Exercise 4

- Créer une nouvelle branche `feature/newpage`
- Ajouter un nouveau theme sur cette branche
- Pusher la branche et créer une pull request vers `develop`

---
# Exercise 4

- Créer une nouvelle branche `feature/add_comments_plugin`
- Ajouter un plugin de commentaire
- Pusher la branche et créer une pull request vers `develop`

---
# Exercise 4

- Créer une release 1.0
